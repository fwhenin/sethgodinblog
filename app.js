var FeedParser = require('feedparser');
var request = require('request');
var Parse = require('parse').Parse;
var schedule = require('node-schedule');
var express = require("express");
var app = express();

Parse.initialize("R1lvdIYmdVRhp29ob5sXQx8ut2LpEWaw6iZfRzcz", "AchDxTRWHm25Qx1SaCa7DGSxzHI0nAlcPNw0KOD9");

function getData(){

    var req = request('http://sethgodin.typepad.com/seths_blog/atom.xml')
        , feedparser = new FeedParser();

    req.on('error', function (error) {
        // handle any request errors
    });
    req.on('response', function (res) {
        var stream = this;

        if (res.statusCode != 200) return this.emit('error', new Error('Bad status code'));

        stream.pipe(feedparser);
    });

    feedparser.on('error', function(error) {
        // always handle errors
    });
    feedparser.on('readable', function() {
        // This is where the action is!
        var stream = this
            //, meta = this.meta // **NOTE** the "meta" is always available in the context of the feedparser instance
            , item;

        while (item = stream.read()) {
            uploadPostToParse(item);
        }
    });

}

function uploadPostToParse(post){
    var BlogPost = Parse.Object.extend("BlogPost");

    var query = new Parse.Query(BlogPost);
    query.equalTo("postId", post.guid);
    query.find({
        success: function(results) {

            if (results.length == 0){
                // Upload Post
                console.log("Saving BlogPost: " + post.title);
                var blogPost = new BlogPost();

                blogPost.set("postId", post.guid);
                blogPost.set("title", post.title);
                blogPost.set("content", post.description);
                blogPost.set("date", post.date);
                blogPost.set("pubDate", post.pubdate);


                blogPost.save(null, {
                    success: function(blogPost) {
                        // Execute any logic that should take place after the object is saved.
                        console.log('New post created with objectId: ' + blogPost.id);
                    },
                    error: function(gameScore, error) {
                        // Execute any logic that should take place if the save fails.
                        // error is a Parse.Error with an error code and message.
                        console.log('Failed to create new object, with error code: ' + error.message);
                    }
                });
            }else{
                // if results date != blog date, we need to update
                var currentPost = results[0];
                var currentDate = Date.parse(currentPost.get("date"));
                var savedDate = Date.parse(post.date);
                if (currentDate !== savedDate){
                    currentPost.set("postId", post.guid);
                    currentPost.set("title", post.title);
                    currentPost.set("content", post.description);
                    currentPost.set("date", post.date);
                    currentPost.set("pubDate", post.pubdate);

                    currentPost.save(null, {
                        success: function(blogPost) {
                            // Execute any logic that should take place after the object is saved.
                            console.log('Post Updated: ' + blogPost.id);
                        },
                        error: function(gameScore, error) {
                            // Execute any logic that should take place if the save fails.
                            // error is a Parse.Error with an error code and message.
                            console.log('Failed to update Post, with error code: ' + error.message);
                        }
                    });
                }else{
                    console.log("everything is up to date")
                }

            }


        },
        error: function(error) {
            console.log("Error: " + error.code + " " + error.message);
        }
    });

}

var rule = new schedule.RecurrenceRule();
//rule.hour = new schedule.Range(10,16);
//rule.dayOfWeek = new schedule.Range(1,5);
rule.minute = new schedule.Range(0, 60, 5);
j = schedule.scheduleJob(rule, function(){
    console.log("Running Script");
    getData();
});

var port = process.env.PORT || 9000;
app.listen(port, function () {
    console.log('Express server listening on %d, in %s mode', port, app.get('env'));
});

app.get('/', function (req, res) {
    res.send('Welcome to Seth Godin Blog');
});
